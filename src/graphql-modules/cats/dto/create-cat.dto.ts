import { CreateCatInput } from '../../../graphql.schema';

export class CreateCatDto extends CreateCatInput {
  name: string;
  age: number;
}
