import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';

import { CatsService } from './cats.service';
import { CreateCatDto } from './dto/create-cat.dto';

import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from '../../common/guards/cats.guard';

import { CurrentUser } from '../../common/decorators/user.decorator';

@Resolver('Cat')
export class CatsResolver {
  constructor(private readonly catsService: CatsService) {}

  @Query()
  @UseGuards(GqlAuthGuard)
  async getCats(@CurrentUser() user: any) {
    console.log('Viendo el usuario: ', user);
    return await this.catsService.findAll();
  }

  @Mutation()
  @UseGuards(GqlAuthGuard)
  async createCat(
    @CurrentUser() user: any,
    @Args('input') input: CreateCatDto,
  ) {
    console.log('Viendo el usuario: ', user);
    return await this.catsService.create(input);
  }
}
