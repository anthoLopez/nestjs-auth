import { Schema, model } from 'mongoose';

export const CatSchema = new Schema(
  {
    name: String,
    age: Number,
  },
  { timestamps: true },
);

export default model('Cat', CatSchema);
