import { Injectable } from '@nestjs/common';

import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Cat } from './interfaces/cats.interface';
import { CreateCatDto } from './dto/create-cat.dto';

@Injectable()
export class CatsService {
  constructor(@InjectModel('Cat') private readonly catModel: Model<Cat>) {}

  async create(input: CreateCatDto): Promise<Cat> {
    const cat = new this.catModel(input);
    return await cat.save();
  }

  async findAll(): Promise<Cat[]> {
    return await this.catModel.find();
  }
}
