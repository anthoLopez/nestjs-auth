import { Module } from '@nestjs/common';
import { CatsService } from './cats.service';
import { CatsResolver } from './cats.resolvers';

import { MongooseModule } from '@nestjs/mongoose';
import { CatSchema } from './schemas/cats.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Cat', schema: CatSchema }])],
  providers: [CatsResolver, CatsService],
  exports: [CatsService],
})
export class CatsModule {}
