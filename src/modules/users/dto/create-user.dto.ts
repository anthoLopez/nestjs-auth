import { ApiModelProperty } from '@nestjs/swagger';

export class CreatUserDto {
  @ApiModelProperty()
  readonly firstname: string;

  @ApiModelProperty()
  readonly lastname: string;

  @ApiModelProperty()
  readonly email: string;

  @ApiModelProperty()
  password: string;

  @ApiModelProperty()
  readonly age: number;
}
