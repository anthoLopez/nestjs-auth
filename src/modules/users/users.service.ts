import { Model } from 'mongoose';
import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { User } from './interfaces/user.interface';
import { CreatUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async create(createUserDto: CreatUserDto): Promise<User> {
    const { email, password } = createUserDto;

    const findUser = await this.findUserByEmail(email);
    if (findUser) throw new NotFoundException('El email ya existe.');

    const createdUser = new this.userModel(createUserDto);
    createdUser.password = await createdUser.encryptPassword(password);
    return await createdUser.save();
  }

  async findUserByEmail(email: string): Promise<any> {
    return await this.userModel.findOne({ email });
  }

  async findAll(): Promise<User[]> {
    return await this.userModel.find();
  }

  async signin(loginUserDto: LoginUserDto): Promise<User> {
    const { email, password } = loginUserDto;

    const user = await this.userModel.findOne({ email });
    if (!user) throw new UnauthorizedException('Credenciales invalidas.');

    const matchPass = await user.matchPassword(password);
    if (!matchPass) throw new UnauthorizedException('Credenciales invalidas.');

    return user;
  }

  async findByPayload(payload: any) {
    const { email } = payload;
    return await this.userModel.findOne({ email });
  }

  async myProfile(userId: string): Promise<User> {
    return await this.userModel.findById(userId);
  }
}
