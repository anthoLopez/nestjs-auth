import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';

import { CreatUserDto } from './dto/create-user.dto';
import { User } from './interfaces/user.interface';

import { User as UserDecorator } from '../../common/decorators/user.decorator';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async create(@Body() createUserDto: CreatUserDto): Promise<User> {
    return await this.usersService.create(createUserDto);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async findAll(@UserDecorator() user: any): Promise<User[]> {
    return await this.usersService.findAll();
  }

  @Get('me')
  @UseGuards(AuthGuard('jwt'))
  async myProfile(@UserDecorator() user: any): Promise<User> {
    return await this.usersService.myProfile(user.id);
  }
}
