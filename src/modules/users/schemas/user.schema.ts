import { Schema, model } from 'mongoose';
import * as bcrypt from 'bcryptjs';

export const UserSchema = new Schema(
  {
    firstname: String,
    lastname: String,
    email: String,
    password: String,
    age: Number,
  },
  { timestamps: true },
);

UserSchema.methods.encryptPassword = async function(
  password: string,
): Promise<string> {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

UserSchema.methods.matchPassword = async function(
  candidatePass: string,
): Promise<boolean> {
  if (!this.password) return;
  return await bcrypt.compare(candidatePass, this.password);
};

export default model('User', UserSchema);
