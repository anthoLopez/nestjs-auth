import { Document } from 'mongoose';

export interface User extends Document {
  readonly firstname: string;
  readonly lastname: string;
  readonly email: string;
  password: string;
  readonly age: number;

  encryptPassword(password: string): Promise<string>;
  matchPassword(newPass: string): Promise<boolean>;
}
