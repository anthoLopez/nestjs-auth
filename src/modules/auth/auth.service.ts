import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { sign } from 'jsonwebtoken';

import { ConfigService } from '../../config/config.service';

import { LoginUserDto } from '../users/dto/login-user.dto';
import { CreatUserDto } from '../users/dto/create-user.dto';
import { Payload } from './interfaces/payload.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private config: ConfigService,
  ) {}

  async validateUser(payload: Payload): Promise<any> {
    return await this.usersService.findByPayload(payload);
  }

  async signPayload(payload: Payload) {
    return sign(payload, this.config.get('SECRET_KEY'), { expiresIn: '1h' });
  }

  async login(loginUserDto: LoginUserDto): Promise<any> {
    const user = await this.usersService.signin(loginUserDto);
    const payload: Payload = { email: user.email };
    const token = await this.signPayload(payload);
    return { user, token };
  }

  async register(createUserDto: CreatUserDto): Promise<any> {
    const user = await this.usersService.create(createUserDto);
    const payload: Payload = { email: user.email };
    const token = await this.signPayload(payload);
    return { user, token };
  }
}
